
package com.develop.animales;

import com.develop.bioma.Bioma;
import java.util.Scanner;
import java.util.ArrayList;

public class Animal {
    String nombre;
    int genero; //(1)Hembra  (2)Macho
    int edad;
    int tipoBioma;   // (1)tundra, (2)sabana, (3)desierto, (4)bosque , (5)taiga
    int estructura;  //  (1)vertebrado o (2)invertebrado
    int tipoAlimentacion; // (1)carnivoro o (2)herbivoro

    
    
    public String anadirGuardarAnimal(Animal animalNuevo){
        animalNuevo.pedirDatos();
        
        return "Ok";
    }
     
    public void pedirDatos(){
        System.out.println("------------------");
        Scanner esc = new Scanner(System.in);
        System.out.println("Nombre del Animal\n");
        setNombre(esc.next()) ;
        System.out.println("Genero del Animal  1)Hembra   2)Macho\n ");
        setGenero(esc.nextInt());
        System.out.println("Edad del Animal\n");
        setEdad(esc.nextInt());
        System.out.println("A que bioma pertenece?  1)Tundra   2)Sabana  3)Desierto  4)Bosque\n");
        setTipoBioma(validacion(1,4));
        System.out.println("Que estuctura tiene?  1)Vertebrado   2)Invertebrado\n");
        setEstructura(validacion(1,2));
        System.out.println("Tipo de alumentacion?  1)Carnivoro   2)Hervivoro  3)omnivoro\n");
        setTipoAlimentacion(validacion(1,3));
    }
    
    public int validacion(int x,int y){
        Scanner esc = new Scanner(System.in);
       int lectura;
        String valor;
        do{
        lectura=esc.nextInt(); 
       valor = (lectura>=x && lectura<=y )?(""):("Error rango no valido... Ingresar de nuevo:\n");
        System.out.println(valor);
        }while(valor!="");
        
        return lectura;
                
    }
    
    public void  verAnimal(){
        System.out.println(getNombre() );
        System.out.println(getEstructura());
        System.out.println(getGenero());
        System.out.println(getEdad());
        System.out.println(getTipoAlimentacion());
        System.out.println(getTipoBioma());
        System.out.println("*********************************");
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getTipoBioma() {
        return tipoBioma;
    }

    public void setTipoBioma(int tipoBioma) {
        this.tipoBioma = tipoBioma;
    }

    public int getEstructura() {
        return estructura;
    }

    public void setEstructura(int estructura) {
        this.estructura = estructura;
    }

    public int getTipoAlimentacion() {
        return tipoAlimentacion;
    }

    public void setTipoAlimentacion(int tipoAlimentacion) {
        this.tipoAlimentacion = tipoAlimentacion;
    }
    
    
    
    
}
