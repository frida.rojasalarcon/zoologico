
package com.develop.principal;

import com.develop.animales.Animal;
import com.develop.bioma.Bioma;
import com.develop.persona.Empleado;
import com.develop.persona.Visitante;
import java.util.Scanner;

import java.util.ArrayList;


public class Menu {
    ArrayList<Animal> animales;
    Animal animalNuevo;
    Empleado empleado;
    Visitante visitante;
    Bioma bioma;
    int totalAnimal;

    public Menu() {
        animales = new ArrayList<Animal>();
    }
    
    
    
    
    public void verMenu(){
        Scanner esc = new Scanner(System.in);
        int opc;
        do{
            System.out.println("¿Que es lo que deseas agregar?"+ "\n" +"1) Animal"+ "\n" +"2) Empreado"+"\n"+"3) Visitante"+ "\n" +"4) Bioma "+ "\n" +"5)Salir\n");
            opc= esc.nextInt();
            switch(opc){
                case 1:
                    animalNuevo= new Animal();
                    animalNuevo.pedirDatos();
                    animales.add(animalNuevo);
                    for (Animal animal : animales) {
                        animal.verAnimal();
                    }
                    break;
                case 2:
                    empleado = new Empleado();
                    empleado = empleado.pedirDatos();
                    empleado.anadirEmpleado(empleado);
                    
                    break;
                case 3:
                    visitante = new Visitante();
                    visitante = visitante.pedirDatos();
                    visitante.anadirVisitante(visitante);
                    break;
                case 4:
                    bioma = new Bioma();
                    bioma = bioma.pedirDatos();
                    bioma.anadirVisitante(bioma);
                    
                    break;
                
                default:break;
            }
            
        }while(opc!=5);
         
        
        
    }
    
}
