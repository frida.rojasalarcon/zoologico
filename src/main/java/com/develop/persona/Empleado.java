
package com.develop.persona;

import com.develop.animales.Animal;
import java.util.Scanner;


public class Empleado {
    String nombre;
    int genero;     //(1)Fenemino  , (2) Masculino
    String rfc;
    String puesto;
    float sueldo;
    String curp;

    
    public void anadirEmpleado(Empleado empleado){
        Empleado dBEmpleado = new Empleado();
        dBEmpleado.nombre = empleado.nombre;
        dBEmpleado.genero = empleado.genero;
        dBEmpleado.rfc= empleado.rfc;
        dBEmpleado.puesto= empleado.puesto;
        dBEmpleado.sueldo= empleado.sueldo;
        dBEmpleado.curp= empleado.curp;
        System.out.println(dBEmpleado.nombre);
    }
    
    public Empleado pedirDatos(){
         Empleado empleado= new Empleado();
        Scanner esc = new Scanner(System.in);
        System.out.println("Nombre del Empleado\n");
        empleado.nombre = esc.next();
        System.out.println("Genero del Empleado  1)Femenino   2)Masculino\n ");
        empleado.genero = esc.nextInt();
        System.out.println("RFC?\n");
        empleado.rfc= esc.next();
        System.out.println("Puesto?  \n");
        empleado.puesto = esc.next();
        System.out.println("Sueldo?  \n");
        empleado.sueldo= esc.nextFloat();
        System.out.println("CUR? \n");
        empleado.curp = esc.next();
        
        return empleado;
        
    }
}
