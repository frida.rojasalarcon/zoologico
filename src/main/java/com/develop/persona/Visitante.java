
package com.develop.persona;

import java.util.Scanner;

public class Visitante {
    String nombre;
    String fechaIngreso;
    int tipoEntrada; // (1)Estandar o  (2)Interactivo
    int tipo;    // (1)Adulto ,  (2)Niño  ,  (3)Estudiante
    int metodoPago;  //(1)Efectivo o (2)Tarjeta   
    
    
    public void anadirVisitante(Visitante visitante){
        Visitante dBVisitante = new Visitante();
        dBVisitante.nombre = visitante.nombre;
        dBVisitante.fechaIngreso = visitante.fechaIngreso;
        dBVisitante.tipoEntrada= visitante.tipoEntrada;
        dBVisitante.tipo= visitante.tipo;
        dBVisitante.metodoPago= visitante.metodoPago;
        System.out.println(dBVisitante.nombre);
        
    }
    
    public Visitante pedirDatos(){
        Visitante visitante= new Visitante();
        Scanner esc = new Scanner(System.in);
        System.out.println("Nombre del Empleado\n");
        visitante.nombre = esc.next();
        System.out.println("Fecha Ingreos\n ");
        visitante.fechaIngreso = esc.next();
        System.out.println("Tipo de entrada  1)Estandar   2)Interactiva\n");
        visitante.tipoEntrada= esc.nextInt();
        System.out.println("Tipo de boleto?  1)Adulto  2)Niño    3)Estudiante  \n");
        visitante.tipo = esc.nextInt();
        System.out.println("Metodo pago?  1)Efectivo   2)Tarjeta\n");
        visitante.metodoPago= esc.nextInt();
        return visitante;
    }
    
}
